//
//  SwiftFrameWorkDemo.swift
//  SwiftFrameWork
//
//  Created by Yudiz Solutions LTD. on 25/11/22.
//

import Foundation
import UIKit

public class SwiftFrameWorkDemo {
    
    public static let shared = SwiftFrameWorkDemo()

    
    public func loadInitialController() -> UIViewController? {
        let bundle = Bundle(for: type(of: self))
        if let controller = UIStoryboard(name: "Entry", bundle: bundle).instantiateInitialViewController() {
            return controller
        }
        return nil
    }
}
