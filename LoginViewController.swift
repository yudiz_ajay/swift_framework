//
//  LoginViewController.swift
//  MyFramework
//
//  Created by Anurag Ajwani on 29/09/2018.
//  Copyright © 2018 Anurag Ajwani. All rights reserved.
//

import UIKit
import SDWebImage

class LoginViewController: UIViewController {

   
    @IBOutlet weak var imgView: UIImageView!
    
//   public init() {
//        super.init(nibName: "LoginViewController", bundle: Bundle(for: LoginViewController.self))
//    }
//
//    required init?(coder aDecoder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imgView.sd_setImage(with: URL(string: "https://source.unsplash.com/user/c_v_r/1900x800")!)
        // Do any additional setup after loading the view.
    }

}
